# Infrastructure

```bash
dotnet new classlib

dotnet add reference "../_3.DataAccessLayer/_3.DataAccessLayer.csproj"
dotnet add reference "../_0.Share/_0.Share.csproj"
#
dotnet build
#
dotnet add package AutoMapper
dotnet add package Microsoft.Extensions.Http
#
```
