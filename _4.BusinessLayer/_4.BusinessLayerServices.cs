﻿using _0.Share;
using _0.Share.IServices;
using _0.Share.IServices.Beta;
using _4.BusinessLayer.Services;
using Microsoft.Extensions.DependencyInjection;

namespace _4.BusinessLayer;

public static partial class ServicesExtensions
{
    public static IServiceCollection AddBusinessLayerServices(
        this IServiceCollection services,
        Appsettings appsettings)
    {
        // add services
        services.AddScoped<IAuthService, AuthService>();
        services.AddScoped<_0.Share.IServices.IUserService, Services.UserService>();
        services.AddScoped<_0.Share.IServices.Beta.IUserService, Services.Beta.UserService>();
        services.AddScoped<IMyProfileService, MyProfileService>();

        return services;
    }
}
