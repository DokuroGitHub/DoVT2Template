using System.Linq.Expressions;
using _0.Share.Commons;
using _0.Share.IServices;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Commons;
using AutoMapper;

namespace _4.BusinessLayer.Services;

public class UserService : IUserService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UserService(
        IUnitOfWork unitOfWork,
        IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public async Task<PagedResult<UserDto>> GetPagedItems(
        string? search = null,
        int pageIndex = 1,
        int pageSize = 10,
        string? fields = null,
        string? format = null)
    {
        var result = await _unitOfWork.UserRepository.GetPagedResultAsync(
            filter: x => string.IsNullOrEmpty(search) || x.FirstName != null && x.FirstName.Contains(search),
            include: x => x
                .MyInclude(x => x.CreatedUsers).MyThenInclude(x => x.CreatedUsers),
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<UserDto>>(result);
        return pagedItems;
    }

    public async Task<PagedResult<UserDto>> AdvancedSearch(
        UserSearch search,
        int pageIndex = 1,
        int pageSize = 10,
        string? fields = null,
        string? format = null)
    {
        Expression<Func<User, bool>> filter = x => true;
        filter = string.IsNullOrEmpty(search.FirstName) ?
            filter :
            filter.AndAlso(x => x.FirstName != null && x.FirstName.Contains(search.FirstName));
        filter = string.IsNullOrEmpty(search.LastName) ?
            filter :
            filter.AndAlso(x => x.LastName != null && x.LastName.Contains(search.LastName));
        filter = string.IsNullOrEmpty(search.Email) ?
            filter :
            filter.AndAlso(x => x.Email != null && x.Email.Contains(search.Email));
        filter = string.IsNullOrEmpty(search.DisplayName) ?
            filter :
            filter.AndAlso(x => x.DisplayName != null && x.DisplayName.Contains(search.DisplayName));

        var items = await _unitOfWork.UserRepository.GetPagedResultAsync(
            filter: filter,
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<UserDto>>(items);
        return pagedItems;
    }

    public async Task<List<UserDto>> GetAll(
        string? fields = null,
        string? format = null)
    {
        var items = await _unitOfWork.UserRepository.GetAllAsync();
        var result = _mapper.Map<List<UserDto>>(items);
        return result;
    }

    public async Task<UserDto?> GetOne(
        int id,
        string? fields = null,
        string? format = null)
    {
        var user = await _unitOfWork.UserRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (user == null)
            return null;
        var result = _mapper.Map<UserDto>(user);
        return result;
    }

    public async Task<UserDto> Create(
        UserCreateDto dto,
        string? fields = null,
        string? format = null)
    {
        var user = _mapper.Map<User>(dto);
        user.CreatedBy = 1;
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't add User, {ex.Message}", ex);
        }
        var result = _mapper.Map<UserDto>(user);
        return result;
    }

    public async Task<ResponseTypeId?> Delete(int id)
    {
        var user = await _unitOfWork.UserRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (user == null)
            return null;
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.UserRepository.RemoveAsync(x => x.Id == id);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete User", ex);
        }
        return new ResponseTypeId { id = id };
    }

    public async Task<UserDto?> Update(
        int id,
        UserUpdateDto dto,
        string? fields = null,
        string? format = null)
    {
        var item = await _unitOfWork.UserRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (item == null)
            return null;
        _mapper.Map(dto, item);
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.UserRepository.Update(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't update User", ex);
        }
        var result = _mapper.Map<UserDto>(item);
        return result;
    }
}
