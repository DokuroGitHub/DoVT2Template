﻿using _0.Share.Commons;
using _0.Share.Enums;
using _0.Share.IServices;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using AutoMapper;

namespace _4.BusinessLayer.Services;

public class AuthService : IAuthService
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IJwtService _jwtService;

    public AuthService(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IJwtService jwtService) : base()
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _jwtService = jwtService;
    }

    public async Task<bool> IsUniqueUserAsync(string username)
    {
        var user = await _unitOfWork.UserRepository.FirstOrDefaultAsync(x => x.Username == username);
        return user == null;
    }

    public async Task<LoginResponse?> LoginAsync(
        LoginRequest dto,
        string? fields = null,
        string? format = null)
    {
        var user = await _unitOfWork.UserRepository.FirstOrDefaultAsync(
            u => u.Username == dto.Username);

        if (user == null)
            return null;

        bool isValid = _jwtService.Verify(dto.Password, user.HashPassword);
        if (!isValid)
            return null;

        //if user was found generate JWT Token
        var currentUser = new CurrentUser()
        {
            UserId = user.Id,
            DisplayName = user.DisplayName,
            Email = user.Email ?? "",
            Role = "User"
        };
        var token = _jwtService.GenerateJWT(currentUser);

        var loginResponseDto = new LoginResponse()
        {
            Token = token,
            CurrentUser = currentUser,
        };
        return loginResponseDto;
    }

    public async Task<UserFlatDto?> RegisterAsync(
        RegisterRequest dto,
        string? fields = null,
        string? format = null)
    {
        var user = new User()
        {
            Email = dto.Email,
            Username = dto.Email,
            HashPassword = BCrypt.Net.BCrypt.HashPassword(dto.Password),
        };
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Error while saving user to database", ex);
        }
        var result = _mapper.Map<UserFlatDto>(user);
        return result;
    }

    // Admin Login
    public async Task<LoginResponse?> AdminLoginAsync(
        LoginRequest dto,
        string? fields = null,
        string? format = null)
    {
        await _unitOfWork.UserRepository.AnyAsync();
        if (dto.Username != "admin" || dto.Password != "admin")
            return null;
        var currentUser = new CurrentUser()
        {
            UserId = 0,
            DisplayName = "Admin",
            Email = "admin@gmail.com",
            Role = UserRole.Admin.ToStringValue()
        };
        var token = _jwtService.GenerateJWT(currentUser);
        var loginResponseDto = new LoginResponse()
        {
            Token = token,
            CurrentUser = currentUser,
        };
        return loginResponseDto;
    }
}
