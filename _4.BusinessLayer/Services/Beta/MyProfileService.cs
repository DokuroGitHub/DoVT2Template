using _0.Share.Commons;
using _0.Share.IServices.Beta;
using _0.Share.ViewModels.User;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using AutoMapper;

namespace _4.BusinessLayer.Services;

public class MyProfileService : IMyProfileService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _currentUserService;

    public MyProfileService(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICurrentUserService currentUserService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _currentUserService = currentUserService;
    }

    public async Task<UserDto?> GetMyProfile(
        string? fields = null,
        string? format = null)
    {
        var user = await _unitOfWork.UserRepository.FirstOrDefaultAsync(x => x.Id == _currentUserService.CurrentUserId);
        if (user == null)
            return null;
        var result = _mapper.Map<UserDto>(user);
        return result;
    }

    public async Task<UserDto> UpdateMyProfile(
        UserUpdateDto dto,
        string? fields = null,
        string? format = null)
    {
        var item = await _unitOfWork.UserRepository.FirstOrDefaultAsync(x => x.Id == _currentUserService.CurrentUserId);
        if (item == null)
            throw new ServiceException("User not found!!!");
        _mapper.Map(dto, item);
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.UserRepository.Update(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't update User", ex);
        }
        var result = _mapper.Map<UserDto>(item);
        return result;
    }
}
