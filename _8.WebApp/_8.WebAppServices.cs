﻿using System.Diagnostics;
using _8.WebApp.Middlewares;
using _8.WebApp.Services;
using _8.WebApp.Services.IServices;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace _8.WebApp;

public static partial class ServicesExtensions
{
    public static IServiceCollection AddWebAppServices(this IServiceCollection services)
    {
        // add controllers and views
        services.AddControllersWithViews();

        services.AddMvc();

        // Add application services.
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        services.AddSingleton<ExceptionMiddleware>();
        services.AddSingleton<PerformanceMiddleware>();
        services.AddSingleton<Stopwatch>();
        //
        // services.AddHttpContextAccessor();
        services.AddScoped<ICookieAuthService, CookieAuthService>();
        services.AddScoped<ICookieTokenService, CookieTokenService>();
        services.AddScoped<ICookieUserDataService, CookieUserDataService>();

        services.Configure<HstsOptions>(options =>
        {
            options.IncludeSubDomains = true;
            options.MaxAge = TimeSpan.FromDays(365);
        });

        services.AddHealthChecks();

        services.AddDistributedMemoryCache();

        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.LoginPath = "/Auth/Login";
                options.AccessDeniedPath = "/Auth/AccessDenied";
                options.SlidingExpiration = true;
            });
        services.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromMinutes(100);
            options.Cookie.HttpOnly = true;
            options.Cookie.IsEssential = true;
        });

        return services;
    }

    public static WebApplication UseWebAppMiddlewares(this WebApplication app)
    {
        app.UseExceptionMiddleware();
        app.UsePerformanceMiddleware();

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{id?}");
        //
        app.MapHealthChecks("/healthcheck");

        return app;
    }
}
