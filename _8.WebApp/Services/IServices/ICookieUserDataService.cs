﻿using _8.WebApp.Models;

namespace _8.WebApp.Services.IServices;

public interface ICookieUserDataService
{
    UserData? GetUserData();
    void SetUserData(UserData userData);
    void ClearUserData();
}
