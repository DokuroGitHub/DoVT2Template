﻿using _0.Share.ViewModels;

namespace _8.WebApp.Services.IServices;

public interface ICookieAuthService
{
    Task SignInAsync(LoginResponse response);
    Task SignOutAsync();
    bool IsAuthenticated();
    bool HasPermission(string permission);
}
