﻿namespace _8.WebApp.Services.IServices;

public interface ICookieTokenService
{
    void SetAccessToken(string accessToken);
    string? GetAccessToken();
    void SetRefreshToken(string refreshToken);
    string? GetRefreshToken();
    void ClearTokens();
}
