﻿using System.Text;
using System.Text.Json;
using _8.WebApp.Models;
using _8.WebApp.Services.IServices;

namespace _8.WebApp.Services;

public class CookieUserDataService : ICookieUserDataService
{
    private readonly HttpContext? _httpContext;

    public CookieUserDataService(
        IHttpContextAccessor httpContextAccessor)
    {
        _httpContext = httpContextAccessor.HttpContext;
    }
    public UserData? GetUserData()
    {
        var userDataString = _httpContext?.Request.Cookies["user_data"];
        System.Console.WriteLine($"userDataString: {userDataString}");

        if (string.IsNullOrEmpty(userDataString))
            return null;

        var userDataBytes = Convert.FromBase64String(userDataString);
        var userDataJson = Encoding.UTF8.GetString(userDataBytes);
        var userData = JsonSerializer.Deserialize<UserData>(userDataJson);

        return userData;
    }

    public void SetUserData(UserData userData)
    {
        var userDataJson = JsonSerializer.Serialize(userData);
        var userDataBytes = Encoding.UTF8.GetBytes(userDataJson);
        var userDataString = Convert.ToBase64String(userDataBytes);
        System.Console.WriteLine($"userDataString: {userDataString}");

        _httpContext?.Response.Cookies.Append("user_data", userDataString, new CookieOptions
        {
            HttpOnly = true,
            Secure = true,
            SameSite = SameSiteMode.Strict,
            Expires = DateTime.UtcNow.AddMinutes(15)
        });
    }

    public void ClearUserData()
    {
        _httpContext?.Response.Cookies.Delete("user_data");
    }
}
