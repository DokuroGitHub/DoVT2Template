﻿using _8.WebApp.Services.IServices;

namespace _8.WebApp.Services;

public class CookieTokenService : ICookieTokenService
{
    private readonly HttpContext? _httpContext;

    public CookieTokenService(
        IHttpContextAccessor httpContextAccessor)
    {
        _httpContext = httpContextAccessor.HttpContext;
    }

    public void SetAccessToken(string token)
    {
        _httpContext?.Response.Cookies.Append("access_token", token, new CookieOptions
        {
            HttpOnly = true,
            Secure = true,
            SameSite = SameSiteMode.Strict,
            Expires = DateTime.UtcNow.AddYears(69)
        });
    }

    public string? GetAccessToken()
    {
        return _httpContext?.Request.Cookies["access_token"];
    }

    public string? GetRefreshToken()
    {
        return _httpContext?.Request.Cookies["refresh_token"];
    }

    public void SetRefreshToken(string token)
    {
        _httpContext?.Response.Cookies.Append("refresh_token", token, new CookieOptions
        {
            HttpOnly = true,
            Secure = true,
            SameSite = SameSiteMode.Strict,
            Expires = DateTime.UtcNow.AddYears(96)
        });
    }

    public void ClearTokens()
    {
        _httpContext?.Response.Cookies.Delete("access_token");
        _httpContext?.Response.Cookies.Delete("refresh_token");
    }
}
