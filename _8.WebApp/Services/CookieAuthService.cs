﻿using System.Security.Claims;
using _0.Share.ViewModels;
using _8.WebApp.Models;
using _8.WebApp.Services.IServices;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace _8.WebApp.Services;

public class CookieAuthService : ICookieAuthService
{
    private readonly HttpContext? _httpContext;
    private readonly ICookieTokenService _cookieTokenService;
    private readonly ICookieUserDataService _cookieUserDataService;

    public CookieAuthService(
        IHttpContextAccessor httpContextAccessor,
        ICookieTokenService cookieTokenService,
        ICookieUserDataService cookieUserDataService)
    {
        _httpContext = httpContextAccessor.HttpContext;
        _cookieTokenService = cookieTokenService;
        _cookieUserDataService = cookieUserDataService;
    }

    public async Task SignInAsync(LoginResponse response)
    {
        _cookieTokenService.SetAccessToken(response.Token);
        var userData = new UserData
        {
            UserId = response.CurrentUser.UserId.ToString(),
            Avatar = "https://i.pravatar.cc/300",
            DisplayName = response.CurrentUser.DisplayName,
            Email = response.CurrentUser.Email,
            Role = response.CurrentUser.Role,
            Permissions = GetPermissionsByRole(response.CurrentUser.Role)
        };
        _cookieUserDataService.SetUserData(userData);
        var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
        identity.AddClaim(new Claim(ClaimTypes.Name, response.CurrentUser.UserId.ToString()));
        identity.AddClaim(new Claim(ClaimTypes.Role, response.CurrentUser.Role));
        var principal = new ClaimsPrincipal(identity);
        if (_httpContext != null)
            await _httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
    }

    public async Task SignOutAsync()
    {
        _cookieTokenService.ClearTokens();
        _cookieUserDataService.ClearUserData();
        if (_httpContext != null)
            await _httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
    }

    public bool IsAuthenticated()
    {
        var token = _cookieTokenService.GetAccessToken();
        System.Console.WriteLine($"token: {token}");
        if (token == null)
            return false;
        var userData = _cookieUserDataService.GetUserData();
        System.Console.WriteLine($"userData.UserId: {userData?.UserId}");
        if (userData == null)
            return false;

        return true;
    }

    public bool HasPermission(string permission)
    {
        var userData = _cookieUserDataService.GetUserData();
        if (userData == null)
            return false;
        // Check if the user has the specified permission
        var permissions = GetPermissionsByRole(userData.Role);
        if (permissions.Contains(permission))
            return true;
        return false;
    }

    private List<string> GetPermissionsByRole(string role)
    {
        var roles = new List<Role>
        {
            new Role
            {
                Name = "User",
                Permissions = new List<string>
                {
                    "ViewDashboard",
                    "EditUserProfile"
                }
            },
            new Role
            {
                Name = "Admin",
                Permissions = new List<string>
                {
                    "ViewDashboard",
                    "CreateUser",
                    "DeleteUser",
                    "EditUserProfile"
                }
            }
        };

        return roles.Where(x => x.Name == role).FirstOrDefault()?.Permissions ?? new List<string>();
    }
}
