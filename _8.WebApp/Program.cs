using _6.ApiWrapper;
using _8.WebApp;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddApiWrapperServices();
builder.Services.AddWebAppServices();

var app = builder.Build();

app.UseWebAppMiddlewares();

app.Run();
