﻿namespace _8.WebApp.Models;

#pragma warning disable
public class Role
{
    public string Name { get; set; }
    public List<string> Permissions { get; set; }
}
