﻿namespace _8.WebApp.Models;

#pragma warning disable
public class UserData
{
    public string UserId { get; set; }
    public string? Avatar { get; set; }
    public string? Email { get; set; }
    public string? DisplayName { get; set; }
    public string UserName { get; set; }
    public string Role { get; set; }
    public List<string> Permissions { get; set; }
}
