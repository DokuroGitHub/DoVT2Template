﻿using Microsoft.AspNetCore.Mvc;

namespace _8.WebApp.Controllers;

public class ErrorController : Controller
{
    public ErrorController()
    {
    }

    [Route("404")]
    [HttpGet]
    public IActionResult PageNotFound()
    {
        string originalPath = "unknown";
        if (HttpContext.Items.ContainsKey("originalPath"))
        {
            originalPath = HttpContext.Items["originalPath"] as string ?? "";
        }
        return View();
    }
}
