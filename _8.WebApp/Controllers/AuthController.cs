﻿using _0.Share.IServices;
using _0.Share.ViewModels;
using _8.WebApp.Services.IServices;
using Microsoft.AspNetCore.Mvc;

namespace _8.WebApp.Controllers;

public class AuthController : Controller
{
    private readonly IAuthService _authService;
    private readonly ICookieAuthService _cookieAuthService;
    private readonly ICookieTokenService _cookieTokenService;
    private readonly ICookieUserDataService _cookieUserDataService;

    public AuthController(
        IAuthService authService,
        ICookieAuthService cookieAuthService,
        ICookieTokenService cookieTokenService,
        ICookieUserDataService cookieUserDataService)
    {
        _authService = authService;
        _cookieAuthService = cookieAuthService;
        _cookieTokenService = cookieTokenService;
        _cookieUserDataService = cookieUserDataService;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [HttpGet]
    public IActionResult Login()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login(LoginRequest dto, string? returnUrl)
    {
        if (ModelState.IsValid)
        {
            var response = await _authService.LoginAsync(dto);
            if (response != null)
            {
                await _cookieAuthService.SignInAsync(response);
                TempData["success"] = "Login successfully.";
                if (returnUrl != null && returnUrl.Length > 0)
                    return Redirect(returnUrl);
                return RedirectToAction("Index", "Home");
            }
        }
        TempData["error"] = "Login failed, please check your credentials.";
        return View(dto);
    }

    [HttpGet]
    public IActionResult Register()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Register(RegisterRequest model)
    {
        if (ModelState.IsValid)
        {
            var response = await _authService.RegisterAsync(model);
            if (response != null)
            {
                TempData["success"] = "Register successfully.";
                return RedirectToAction(nameof(Login));
            }
        }
        TempData["error"] = "Encountered error.";
        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> Logout()
    {
        await _cookieAuthService.SignOutAsync();
        return RedirectToAction("Index", "Home");
    }

    [HttpGet]
    public IActionResult AccessDenied()
    {
        return View();
    }
}
