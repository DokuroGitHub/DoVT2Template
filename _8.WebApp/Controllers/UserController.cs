﻿using _0.Share.IServices;
using _0.Share.ViewModels.User;
using Microsoft.AspNetCore.Mvc;

namespace _8.WebApp.Controllers;

public class UserController : Controller
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var items = await _userService.GetAll();
        return View(items);
    }

    [HttpGet]
    public async Task<IActionResult> Details(int id)
    {
        var item = await _userService.GetOne(id);
        if (item != null)
            return View(item);
        return NotFound();
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(UserCreateDto model)
    {
        if (ModelState.IsValid)
        {
            var item = await _userService.Create(model);
            if (item != null)
            {
                TempData["success"] = "Created successfully.";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ModelState.AddModelError("ErrorMessages", "Create failed.");
            }
        }
        TempData["error"] = "Encountered error.";
        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        var item = await _userService.GetOne(id);
        if (item != null)
        {
            TempData["id"] = id;
            return View(item);
        }
        return NotFound();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, UserUpdateDto model)
    {
        if (ModelState.IsValid)
        {
            var item = await _userService.Update(id, model);
            if (item != null)
            {
                TempData["success"] = "Updated successfully.";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ModelState.AddModelError("ErrorMessages", "Update failed.");
            }
        }
        TempData["error"] = "Encountered error.";
        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> Delete(int id)
    {
        var item = await _userService.GetOne(id);
        if (item != null)
            return View(item);
        return NotFound();
    }

    [HttpPost]
    public async Task<IActionResult> Delete(UserDto model)
    {
        var item = await _userService.Delete(model.Id);
        if (item != null)
        {
            TempData["success"] = "Deleted successfully.";
            return RedirectToAction(nameof(Index));
        }
        else
        {
            ModelState.AddModelError("ErrorMessages", "Delete failed.");
        }
        TempData["error"] = "Encountered error.";
        return View(model);
    }
}
