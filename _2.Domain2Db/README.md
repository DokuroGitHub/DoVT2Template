# \_2.Domain2Db

Edit ConnectionStrings

```json
  "ConnectionStrings": {
    "DefaultSQLConnectionString": "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DoVT2Template;Integrated Security=True;Encrypt=False"
  },

  "ConnectionStrings": {
    "DefaultSQLConnectionString": "Data Source=DESKTOP-2T86NCF;Initial Catalog=DoVT2Template;Integrated Security=True;Encrypt=False"
  },
```

migrations

```bash
#
dotnet build
dotnet ef migrations add InitDb
dotnet ef database update
#
```

```bash
dotnet new classlib

dotnet add reference "../_1.Domain/_1.Domain.csproj"

dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.Extensions.Configuration.Json
dotnet add package Microsoft.Extensions.Configuration.Binder
dotnet add package Newtonsoft.Json
#

```
