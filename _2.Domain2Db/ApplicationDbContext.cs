﻿using System.Reflection;
using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace _2.Domain2Db;

public class ApplicationDbContext : DbContext
{
#pragma warning disable
    public ApplicationDbContext(DbContextOptions options) : base(options) { }
#pragma warning restore

    public DbSet<User> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // FluentApis
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        // seed
        modelBuilder.Seed();
    }
}
