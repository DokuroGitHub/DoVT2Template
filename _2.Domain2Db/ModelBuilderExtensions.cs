using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace _2.Domain2Db;

public static class ModelBuilderExtensions
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        if (Directory.Exists("Seeds"))
        {
            modelBuilder.GenericSeed<User>("Seeds/User.json");
        }
    }

    public static void GenericSeed<T>(this ModelBuilder modelBuilder, string path) where T : class
    {
        using var r = new StreamReader(path);
        string json = r.ReadToEnd();
        var items = JsonConvert.DeserializeObject<List<T>>(json);
        if (items != null)
        {
            modelBuilder.Entity<T>().HasData(items);
        }
    }
}
