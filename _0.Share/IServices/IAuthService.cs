﻿using _0.Share.ViewModels;
using _0.Share.ViewModels.User;

namespace _0.Share.IServices;

public interface IAuthService
{
    Task<bool> IsUniqueUserAsync(string username);
    Task<LoginResponse?> LoginAsync(
        LoginRequest dto,
        string? fields = null,
        string? format = null);
    Task<UserFlatDto?> RegisterAsync(
        RegisterRequest dto,
        string? fields = null,
        string? format = null);

    // Admin Login
    Task<LoginResponse?> AdminLoginAsync(
        LoginRequest dto,
        string? fields = null,
        string? format = null);
}
