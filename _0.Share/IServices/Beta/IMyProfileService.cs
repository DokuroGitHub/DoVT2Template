using _0.Share.ViewModels.User;

namespace _0.Share.IServices.Beta;

public interface IMyProfileService
{
    Task<UserDto?> GetMyProfile(
        string? fields = null,
        string? format = null);
    Task<UserDto> UpdateMyProfile(
        UserUpdateDto dto,
        string? fields = null,
        string? format = null);
}
