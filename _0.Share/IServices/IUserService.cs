using _0.Share.ViewModels;
using _0.Share.ViewModels.User;

namespace _0.Share.IServices;

public interface IUserService
{
    Task<PagedResult<UserDto>> GetPagedItems(
        string? search = null,
        int pageIndex = 1,
        int pageSize = 10,
        string? fields = null,
        string? format = null);
    Task<PagedResult<UserDto>> AdvancedSearch(
        UserSearch search,
        int pageIndex = 1,
        int pageSize = 10,
        string? fields = null,
        string? format = null);
    Task<List<UserDto>> GetAll(
        string? fields = null,
        string? format = null);
    Task<UserDto?> GetOne(
        int id,
        string? fields = null,
        string? format = null);
    Task<UserDto> Create(
        UserCreateDto dto,
        string? fields = null,
        string? format = null);
    Task<ResponseTypeId?> Delete(int id);
    Task<UserDto?> Update(
        int id,
        UserUpdateDto dto,
        string? fields = null,
        string? format = null);
}
