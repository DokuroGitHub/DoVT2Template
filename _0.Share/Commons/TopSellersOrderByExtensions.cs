﻿
using _0.Share.Enums;

namespace _0.Share.Commons;

public static class TopSellersOrderByExtensions
{
    public static string ToStringValue(this TopSellersOrderBy orderBy)
    {
        switch (orderBy)
        {
            case TopSellersOrderBy.OrderCount:
                return "orderCount";
            case TopSellersOrderBy.SoldQuantity:
                return "soldQuantity";
            case TopSellersOrderBy.TotalPrice:
            default:
                return "totalPrice";
        }
    }

    public static TopSellersOrderBy ToTopSellersOrderBy(this string? str)
    {
        switch (str)
        {
            case "orderCount":
                return TopSellersOrderBy.OrderCount;
            case "soldQuantity":
                return TopSellersOrderBy.SoldQuantity;
            case "totalPrice":
            default:
                return TopSellersOrderBy.TotalPrice;
        }
    }
}
