﻿
using _0.Share.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace _0.Share.Commons;

public static class FormatExtensions
{
    public static string ToStringValue(this Format format)
    {
        switch (format)
        {
            case Format.CamelCase:
                return "camel";
            case Format.TitleCase:
                return "title";
            case Format.KebabCase:
                return "kebab";
            default:
                throw new ArgumentOutOfRangeException(nameof(format), format, "Invalid format value.");
        }
    }

    public static Format? ToFormat(this string str)
    {
        switch (str)
        {
            case "camel":
                return Format.CamelCase;
            case "title":
                return Format.TitleCase;
            case "kebab":
                return Format.KebabCase;
            default:
                return null;
        }
    }

    public static NamingStrategy? ToNamingStrategy(this string? str)
    {
        switch (str?.ToFormat())
        {
            case Format.CamelCase:
                return new CamelCaseNamingStrategy();
            case Format.TitleCase:
                return new DefaultNamingStrategy();
            case Format.KebabCase:
                return new KebabCaseNamingStrategy();
            case Format.Snake:
                return new SnakeCaseNamingStrategy();
            default:
                return null;
        }
    }

    public static JsonSerializerSettings? ToJsonSerializerSettings(this string? str)
    {
        return str == null ? null : new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = str.ToNamingStrategy()
            }
        };
    }
}
