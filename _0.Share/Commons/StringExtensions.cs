﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace _0.Share.Commons;

public static class StringExtensions
{
    /// <summary> "orderItems" => "OrderItems" </summary>
    public static string ToUpperFirstLetter(this string str)
    {
        if (str.Length == 0) return str;
        var word = str.Length > 1 ? char.ToUpperInvariant(str[0]) + str.Substring(1) :
            char.ToUpperInvariant(str[0]).ToString();
        return word;
    }

    /// <summary> "ORDERITEMS", "orderItems", "OrderItems" => "Orderitems" </summary>
    public static string ToUpperFirstLetterOnly(this string str)
    {
        if (str.Length == 0) return str;
        var word = str.Length > 1 ? char.ToUpperInvariant(str[0]) + str.Substring(1).ToLower() :
            char.ToUpperInvariant(str[0]).ToString();
        return word;
    }

    /// <summary> "order-items", "order_items", " order items " => "OrderItems" </summary>
    public static string ToUpperFirstLetterCase(this string str)
    {
        string[] words = str.Split('-', '_', ' '); // split by hyphen or underscore or space
        for (int i = 0; i < words.Length; i++)
        {
            var word = words[i];
            // if (word.Length == 0) continue;
            words[i] = word.ToUpperFirstLetter();
        }
        string name = string.Join("", words);
        return name;
    }

    /// <summary>
    /// "order-items", "order_items", " order items ", " ORDER ITEMS" => "OrderItems"
    /// "OrderItems" => "Orderitems"
    /// </summary>
    public static string ToTitleCase(this string str)
    {
        string[] words = str.Split('-', '_', ' '); // split by hyphen or underscore or space
        for (int i = 0; i < words.Length; i++)
        {
            // words[i] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(words[i]);
            var word = words[i];
            words[i] = word.ToUpperFirstLetterOnly();
        }
        string name = string.Join("", words);
        return name;
    }

    /// <summary> "order-items", "order_items", " order items " => "orderItems" </summary>
    public static string ToCamelCase(this string str)
    {
        var name = str.ToUpperFirstLetterCase();
        name = char.ToLowerInvariant(name[0]) + name.Substring(1); // modify first character to lowercase
        return name;
    }

    /// <summary> "OrderItems", "order-items", "order_items", " order items " => "order-items" </summary>
    public static string ToKebabCase(this string str)
    {
        string[] words = Regex.Split(str, @"(?<!^)(?=[A-Z])"); // split by capital letters
        for (int i = 0; i < words.Length; i++)
        {
            // words[i] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(words[i]);
            words[i] = words[i].ToLower();
        }
        string name = string.Join("-", words);
        // name = name.ToLowerInvariant().Replace(" ", "-"); // modify to kebab case
        return name;
    }

    public static string GetUriWithQueryString(
        this string requestUri,
        Dictionary<string, object?> queryStringParams)
    {
        bool startingQuestionMarkAdded = false;
        var sb = new StringBuilder();
        sb.Append(requestUri);
        foreach (var parameter in queryStringParams)
        {
            if (parameter.Value == null)
            {
                continue;
            }
            sb.Append(startingQuestionMarkAdded ? '&' : '?');
            sb.Append(parameter.Key);
            sb.Append('=');
            sb.Append(parameter.Value);
            startingQuestionMarkAdded = true;
        }
        return sb.ToString();
    }

    public static bool IsValidEmail(this string? email)
    {
        if (string.IsNullOrWhiteSpace(email))
            return false;
        try
        {
            // Normalize the domain
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                 RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examines the domain part of the email and normalizes it.
            string DomainMapper(Match match)
            {
                // Use IdnMapping class to convert Unicode domain names.
                var idn = new IdnMapping();
                // Pull out and process domain name (throws ArgumentException on invalid)
                string domainName = idn.GetAscii(match.Groups[2].Value);
                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
        catch (ArgumentException)
        {
            return false;
        }

        try
        {
            return Regex.IsMatch(email,
                @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }
}
