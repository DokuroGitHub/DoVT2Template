using System.Dynamic;
using _0.Share.Enums;

namespace _0.Share.Commons;

public class Utilities
{
    public static object? CreateAnonymousObject<T>(
        T obj,
        string? selectedFields = null,
        string? format = null,
        bool acceptNull = false,
        bool fromUpperFirstLetterCase = true,
        bool fromTitleCase = true,
        bool fromCamelCase = true)
    {
        if (selectedFields == null && format == null)
            return obj;
        var selectedProperties = selectedFields == null ?
            typeof(T).GetProperties() :
            selectedFields.Split(',').Select(name =>
            {
                // "OrderItems", "orderItems", "order_items", " order items " => "OrderItems"
                var UpperFirstLetterCaseProp = fromUpperFirstLetterCase ? typeof(T).GetProperty(name.ToUpperFirstLetterCase()) : null;
                // " ORDER ITEMS " => "OrderItems"
                var TitleCaseProp = fromTitleCase ? typeof(T).GetProperty(name.ToTitleCase()) : null;
                // "orderItems", "order_items", " order items " => "orderItems"
                var camelCaseProp = fromCamelCase ? typeof(T).GetProperty(name.ToCamelCase()) : null;
                return UpperFirstLetterCaseProp ?? TitleCaseProp ?? camelCaseProp;
            });
        var anonymousObject = new ExpandoObject() as IDictionary<string, object?>;
        foreach (var prop in selectedProperties)
        {
            if (prop != null)
            {
                var name = prop.Name;
                switch (format?.ToFormat())
                {
                    case Format.CamelCase:
                        name = name.ToCamelCase();
                        break;
                    case Format.TitleCase:
                        name = name.ToTitleCase();
                        break;
                    case Format.KebabCase:
                        name = name.ToKebabCase();
                        break;
                    default:
                        break;
                }
                var value = prop.GetValue(obj);
                if (value == null && !acceptNull)
                    continue;
                anonymousObject.Add(name, value);
            }
        }

        return anonymousObject;
    }
}
