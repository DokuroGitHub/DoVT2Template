namespace _0.Share.Commons;

public class ServiceException : Exception
{
    public ServiceException(string message) : base(message)
    {
    }

    public ServiceException() : base()
    {
    }

    public ServiceException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}