﻿using _0.Share.Enums;

namespace _0.Share.Commons;

public static class OrderStatusExtensions
{
    public static string ToStringValue(this OrderStatus orderBy)
    {
        switch (orderBy)
        {
            case OrderStatus.Delivering:
                return "delivering";
            case OrderStatus.Delivered:
                return "delivered";
            case OrderStatus.Pending:
            default:
                return "pending";
        }
    }

    public static string? ToStringValueOrDefault(this OrderStatus orderBy)
    {
        switch (orderBy)
        {
            case OrderStatus.Delivering:
                return "delivering";
            case OrderStatus.Delivered:
                return "delivered";
            case OrderStatus.Pending:
                return "pending";
            default:
                return null;
        }
    }

    public static OrderStatus ToOrderStatus(this string? str)
    {
        switch (str)
        {
            case "delivering":
                return OrderStatus.Delivering;
            case "delivered":
                return OrderStatus.Delivered;
            case "pending":
            default:
                return OrderStatus.Pending;
        }
    }

    public static OrderStatus? ToOrderStatusOrDefault(this string? str)
    {
        switch (str)
        {
            case "delivering":
                return OrderStatus.Delivering;
            case "delivered":
                return OrderStatus.Delivered;
            case "pending":
                return OrderStatus.Pending;
            default:
                return null;
        }
    }
}
