namespace _0.Share;

#pragma warning disable
public class Appsettings
{
    public ConnectionStrings ConnectionStrings { get; set; }
    public Jwt Jwt { get; set; }
}

public class ConnectionStrings
{
    public string DefaultSQLConnectionString { get; set; }
}

public class Jwt
{
    public string Key { get; set; }
    public string Issuer { get; set; }
    public string Audience { get; set; }
}
