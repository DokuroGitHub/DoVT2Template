namespace _0.Share.Enums;

public enum TopSellersOrderBy
{
    OrderCount,
    SoldQuantity,
    TotalPrice,
}
