namespace _0.Share.Enums;

public enum Format
{
    CamelCase,
    TitleCase,
    KebabCase,
    Snake,
}

