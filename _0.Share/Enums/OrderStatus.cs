namespace _0.Share.Enums;

public enum OrderStatus
{
    Pending,
    Delivering,
    Delivered
}
