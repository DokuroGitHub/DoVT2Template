
namespace _0.Share.ViewModels.User;

#pragma warning disable
public class UserDto
{
    public int Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public decimal Money { get; set; }
    public DateTime CreatedAt { get; set; }
    public int CreatedBy { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public int? UpdatedBy { get; set; }
    public string Role { get; set; }
    public int? ManagerId { get; set; }
    // ghost
    public string DisplayName { get; set; }
    // ref
    public virtual UserFlatRefDto Creator { get; set; }
    public virtual UserFlatRefDto? Updater { get; set; }
    public virtual UserFlatRefDto? Manager { get; set; }
    public virtual ICollection<UserFlatRefDto> CreatedUsers { get; set; }
    public virtual ICollection<UserFlatRefDto> UpdatedUsers { get; set; }
    public virtual ICollection<UserFlatRefDto> Employees { get; set; }
}
