namespace _0.Share.ViewModels.User;

#pragma warning disable
public class UserUpdatePasswordDto
{
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
}
