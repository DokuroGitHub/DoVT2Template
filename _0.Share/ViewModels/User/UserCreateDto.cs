
namespace _0.Share.ViewModels.User;

#pragma warning disable
public class UserCreateDto
{
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public string Username { get; set; }
    public string HashPassword { get; set; }
    public string? Role { get; set; }
    public int? ManagerId { get; set; }
}
