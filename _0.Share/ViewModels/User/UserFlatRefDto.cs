
namespace _0.Share.ViewModels.User;

#pragma warning disable
public class UserFlatRefDto
{
    public int Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public decimal Money { get; set; }
    public DateTime CreatedAt { get; set; }
    public int CreatedBy { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public int? UpdatedBy { get; set; }
    public string Role { get; set; }
    public int? ManagerId { get; set; }
    // ghost
    public string DisplayName { get; set; }
    // ref
    public virtual UserFlatDto Creator { get; set; }
    public virtual UserFlatDto? Updater { get; set; }
    public virtual UserFlatDto? Manager { get; set; }
    public virtual ICollection<UserFlatDto> CreatedUsers { get; set; }
    public virtual ICollection<UserFlatDto> UpdatedUsers { get; set; }
    public virtual ICollection<UserFlatDto> Employees { get; set; }
}
