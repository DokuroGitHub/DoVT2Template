namespace _0.Share.ViewModels.User;

public class UserSearch
{
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    // ghost
    public string? DisplayName { get; set; }
}
