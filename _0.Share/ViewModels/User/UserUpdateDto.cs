
namespace _0.Share.ViewModels.User;

#pragma warning disable
public class UserUpdateDto
{
    public int? Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
}
