namespace _0.Share.ViewModels;

#pragma warning disable
public class ReportUsersByStartEnd
{
    public DateTime Start { get; set; }
    public DateTime End { get; set; }
    public int UserCount { get; set; }
    public List<ReportUsersByDate> ReportUsersByDates { get; set; }
}
