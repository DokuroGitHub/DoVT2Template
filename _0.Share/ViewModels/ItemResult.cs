﻿using System.Text.Json;

namespace _0.Share.ViewModels;

public class ItemResult<T>
{
    public T? Item { get; set; }
    public int TotalCount { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int TotalPages { get; set; }
    public bool HasPreviousPage => PageIndex > 1;
    public bool HasNextPage => PageIndex < TotalPages;

    public ItemResult()
    {
        Item = default;
        TotalCount = 0;
        PageIndex = 1;
        PageSize = 10;
    }

    public ItemResult(T item, int count, int pageIndex, int pageSize)
    {
        Item = item;
        TotalCount = count;
        PageIndex = pageIndex;
        PageSize = pageSize;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
    }

    public override string ToString()
    {
        return JsonSerializer.Serialize(new
        {
            TotalCount,
            PageIndex,
            PageSize,
            TotalPages,
            HasPreviousPage,
            HasNextPage
        });
    }
}
