﻿namespace _0.Share.ViewModels;

#pragma warning disable
public class LoginResponse
{
    public CurrentUser CurrentUser { get; set; }
    public string Token { get; set; }
}
