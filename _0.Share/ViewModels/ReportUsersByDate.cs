namespace _0.Share.ViewModels;

#pragma warning disable
public class ReportUsersByDate
{
    public DateTime Date { get; set; }
    public int UserCount { get; set; }
}
