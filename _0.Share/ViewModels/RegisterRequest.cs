﻿namespace _0.Share.ViewModels;

#pragma warning disable
public class RegisterRequest
{
    public string Email { get; set; }
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
}
