namespace _0.Share.ViewModels;

#pragma warning disable
public class ResponseTypeId
{
    public int id { get; set; }
}

public class ResponseTypeOrderIdProductId
{
    public int orderId { get; set; }
    public int productId { get; set; }
}

public class ResponseTypeUserId
{
    public int userId { get; set; }
}
