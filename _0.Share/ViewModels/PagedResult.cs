﻿using System.Text.Json;

namespace _0.Share.ViewModels;

public class PagedResult<T>
{
    public List<T> Items { get; set; }
    public int TotalCount { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int TotalPages { get; set; }
    public bool HasPreviousPage => PageIndex > 1;
    public bool HasNextPage => PageIndex < TotalPages;

    public PagedResult()
    {
        Items = new List<T>();
        TotalCount = 0;
        PageIndex = 1;
        PageSize = 10;
    }

    public PagedResult(List<T> items, int count, int pageIndex, int pageSize)
    {
        Items = items;
        TotalCount = count;
        PageIndex = pageIndex;
        PageSize = pageSize;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
    }

    public override string ToString()
    {
        return JsonSerializer.Serialize(new
        {
            TotalCount,
            PageIndex,
            PageSize,
            TotalPages,
            HasPreviousPage,
            HasNextPage
        });
    }
}
