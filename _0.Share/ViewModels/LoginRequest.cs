﻿namespace _0.Share.ViewModels;

#pragma warning disable
public class LoginRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
}
