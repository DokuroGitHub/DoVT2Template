﻿namespace _0.Share.ViewModels;

#pragma warning disable
public class CurrentUser
{
    public int UserId { get; set; }
    public string DisplayName { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
}
