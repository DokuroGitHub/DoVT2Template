namespace _7.ApiClient;

#pragma warning disable
public class Appsettings
{
    public Urls Urls { get; set; }
}

public class Urls
{
    public string BaseUrl { get; set; }
    public string ApiUrl { get; set; }
}
