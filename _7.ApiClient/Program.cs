using _6.ApiWrapper;
using _7.ApiClient;

var builder = WebApplication.CreateBuilder(args);

// appsettings.json
var _appsettings = builder.Configuration.Get<Appsettings>() ?? new Appsettings();
// services
builder.Services.AddApiWrapperServices();
builder.Services.AddApiClientServices(_appsettings);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
