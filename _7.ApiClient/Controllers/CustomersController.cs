using _0.Share.Commons;
using _0.Share.IServices;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using Microsoft.AspNetCore.Mvc;

namespace _7.ApiClient.Controllers;

[ApiController]
[Route("[controller]")]
public class UsersController : ControllerBase
{
    private IUserService _productService;

    public UsersController(IUserService productService)
    {
        _productService = productService;
    }

    [HttpGet("paged-items")]
    [ProducesResponseType(typeof(PagedResult<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetPagedItems(
        [FromQuery] string? search = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _productService.GetPagedItems(
            search: search,
            pageIndex: pageIndex,
            pageSize: pageSize,
            fields: fields,
            format: format);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());

        var pagedResult = new PagedResult<object?>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields, format)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("advanced-search")]
    [ProducesResponseType(typeof(PagedResult<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> AdvancedSearch(
        [FromQuery] string? f = null,
        [FromQuery] string? l = null,
        [FromQuery] string? e = null,
        [FromQuery] string? d = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _productService.AdvancedSearch(
            search: new UserSearch
            {
                FirstName = f,
                LastName = l,
                Email = e,
                DisplayName = d,
            },
            pageIndex: pageIndex,
            pageSize: pageSize,
            fields: fields,
            format: format);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        var pagedResult = new PagedResult<object?>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields, format)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }
}
