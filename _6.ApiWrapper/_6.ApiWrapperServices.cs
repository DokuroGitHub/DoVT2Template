﻿using _0.Share.IServices;
using _6.ApiWrapper.Services;
using Microsoft.Extensions.DependencyInjection;

namespace _6.ApiWrapper;

public static partial class ServicesExtensions
{
    public static IServiceCollection AddApiWrapperServices(
        this IServiceCollection services)
    {
        // add services
        services.AddHttpClient<IAuthService, AuthService>();
        services.AddScoped<IAuthService, AuthService>();
        services.AddHttpClient<IUserService, UserService>();
        services.AddScoped<IUserService, UserService>();

        return services;
    }
}
