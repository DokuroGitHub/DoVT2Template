﻿using System.Text;
using _0.Share.Commons;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using Newtonsoft.Json;

namespace _6.ApiWrapper.Services;

public class UserService : _0.Share.IServices.IUserService
{
    private string _baseUrl { get; set; }
    private HttpClient _client { get; set; }

    public UserService(
        IHttpClientFactory httpClientFactory
    )
    {
        _client = httpClientFactory.CreateClient();
        _baseUrl = "https://localhost:7001/api/v1/Users";
    }

    public async Task<PagedResult<UserDto>> GetPagedItems(
        string? search = null,
        int pageIndex = 1,
        int pageSize = 10,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["search"] = search,
            ["pageIndex"] = pageIndex,
            ["pageSize"] = pageSize,
            ["fields"] = fields,
            ["format"] = format
        };
        var uri = $"{_baseUrl}/paged-items".GetUriWithQueryString(query);
        var response = await _client.GetAsync(uri);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("No items found");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<PagedResult<UserDto>>(content, format.ToJsonSerializerSettings());
        if (result == null)
            throw new ServiceException("No items found");
        return result;
    }

    public async Task<PagedResult<UserDto>> AdvancedSearch(
        UserSearch search,
        int pageIndex = 1,
        int pageSize = 10,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["f"] = search.FirstName,
            ["l"] = search.LastName,
            ["e"] = search.Email,
            ["d"] = search.DisplayName,
            ["pageIndex"] = pageIndex,
            ["pageSize"] = pageSize,
            ["fields"] = fields,
            ["format"] = format
        };
        var response = await _client.GetWithQueryStringAsync($"{_baseUrl}/advanced-search", query);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("No items found");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<PagedResult<UserDto>>(content, format.ToJsonSerializerSettings());
        if (result == null)
            throw new ServiceException("No items found");
        return result;
    }

    public async Task<List<UserDto>> GetAll(
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var response = await _client.GetWithQueryStringAsync(_baseUrl, query);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("No items found");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<List<UserDto>>(content, format.ToJsonSerializerSettings());
        if (result == null)
            throw new ServiceException("No items found");
        return result;
    }

    public async Task<UserDto?> GetOne(
        int id,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var response = await _client.GetWithQueryStringAsync($"{_baseUrl}/{id}", query);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("No item found");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<UserDto>(content, format.ToJsonSerializerSettings());
        return result;
    }

    public async Task<UserDto> Create(
        UserCreateDto dto,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var uri = _baseUrl.GetUriWithQueryString(query);
        var stringContent = new StringContent(
            JsonConvert.SerializeObject(dto, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
            Encoding.UTF8, "application/json");
        var response = await _client.PostAsync(uri, stringContent);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("Can not create item");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<UserDto>(content, format.ToJsonSerializerSettings());
        if (result == null)
            throw new ServiceException("Can not create item");
        return result;
    }

    public async Task<ResponseTypeId?> Delete(int id)
    {
        var response = await _client.DeleteAsync($"{_baseUrl}/{id}");
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("No item found");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<ResponseTypeId>(content);
        if (result == null)
            throw new ServiceException("Can not delete item");
        return result;
    }

    public async Task<UserDto?> Update(
        int id,
        UserUpdateDto dto,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var uri = $"{_baseUrl}/{id}".GetUriWithQueryString(query);
        var stringContent = new StringContent(
       JsonConvert.SerializeObject(dto, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
       Encoding.UTF8, "application/json");
        var response = await _client.PutAsync(uri, stringContent);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("Can not update item");
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<UserDto>(content, format.ToJsonSerializerSettings());
        if (result == null)
            throw new ServiceException("Can not update item");
        return result;
    }
}
