﻿using System.Text;
using _0.Share.Commons;
using _0.Share.IServices;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using Newtonsoft.Json;

namespace _6.ApiWrapper.Services;

public class AuthService : IAuthService
{
    private string _baseUrl { get; set; }
    private HttpClient _client { get; set; }

    public AuthService(
        IHttpClientFactory httpClientFactory
    )
    {
        _client = httpClientFactory.CreateClient();
        _baseUrl = "https://localhost:7001/api/v1/Auth";
    }

    public Task<bool> IsUniqueUserAsync(string username)
    {
        throw new NotImplementedException();
    }

    public async Task<LoginResponse?> LoginAsync(
        LoginRequest dto,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var uri = $"{_baseUrl}/login".GetUriWithQueryString(query);
        var stringContent = new StringContent(
       JsonConvert.SerializeObject(dto, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
       Encoding.UTF8, "application/json");
        var response = await _client.PostAsync(uri, stringContent);
        if (!response.IsSuccessStatusCode)
            return null;
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<LoginResponse>(content, format.ToJsonSerializerSettings());
        return result;
    }

    public async Task<UserFlatDto?> RegisterAsync(
        RegisterRequest dto,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var uri = $"{_baseUrl}/register".GetUriWithQueryString(query);
        var stringContent = new StringContent(
       JsonConvert.SerializeObject(dto, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
       Encoding.UTF8, "application/json");
        var response = await _client.PostAsync(uri, stringContent);
        if (!response.IsSuccessStatusCode)
            return null;
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<UserFlatDto>(content, format.ToJsonSerializerSettings());
        return result;
    }

    public async Task<LoginResponse?> AdminLoginAsync(
        LoginRequest dto,
        string? fields = null,
        string? format = null)
    {
        var query = new Dictionary<string, object?>()
        {
            ["fields"] = fields,
            ["format"] = format
        };
        var uri = $"{_baseUrl}/admin-login".GetUriWithQueryString(query);
        var stringContent = new StringContent(
       JsonConvert.SerializeObject(dto, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
       Encoding.UTF8, "application/json");
        var response = await _client.PostAsync(uri, stringContent);
        if (!response.IsSuccessStatusCode)
            return null;
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<LoginResponse>(content, format.ToJsonSerializerSettings());
        return result;
    }
}
