using _0.Share.ViewModels.User;
using FluentValidation;

namespace _5.Api.Validations;

public class UserSearchValidation : AbstractValidator<UserSearch>
{
    public UserSearchValidation()
    {
        RuleFor(x => x.FirstName)
            .NotEmpty().WithMessage("FirstName cannot be empty.")
            .Length(1, 100).WithMessage("FirstName must be between 1 and 100 characters.");

        RuleFor(x => x.LastName)
            .NotEmpty().WithMessage("LastName cannot be empty.")
            .Length(1, 100).WithMessage("LastName must be between 1 and 100 characters.");

        RuleFor(x => x.DisplayName)
            .NotEmpty().WithMessage("DisplayName cannot be empty.")
            .Length(1, 100).WithMessage("DisplayName must be between 1 and 100 characters.");
    }
}
