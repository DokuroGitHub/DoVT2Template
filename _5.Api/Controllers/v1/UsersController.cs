using _0.Share.Commons;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers.v1;

[ApiController]
[ApiVersion("1.0", Deprecated = true)]
[Route("api/v{version:apiVersion}/[controller]")]
public class UsersController : ControllerBase
{
    private readonly _0.Share.IServices.IUserService _userService;
    private readonly IMapper _mapper;

    public UsersController(
        _0.Share.IServices.IUserService userService,
        IMapper mapper)
    {
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet("paged-items")]
    [ResponseCache(CacheProfileName = "Default30")]
    [ProducesResponseType(typeof(PagedResult<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetPagedItems(
        [FromQuery] string? fields = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1,
        [FromQuery] string? format = null)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _userService.GetPagedItems(
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());

        var pagedResult = new PagedResult<object?>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields, format: format)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("advanced-search")]
    [ProducesResponseType(typeof(PagedResult<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> AdvancedSearch(
        [FromQuery] string? f = null,
        [FromQuery] string? l = null,
        [FromQuery] string? e = null,
        [FromQuery] string? d = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _userService.AdvancedSearch(
            search: new UserSearch
            {
                FirstName = f,
                LastName = l,
                Email = e,
                DisplayName = d,
            },
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        var pagedResult = new PagedResult<object?>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields, format)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet]
    [ProducesResponseType(typeof(List<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAll(
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        var items = await _userService.GetAll();
        return Ok(items.Select(x => Utilities.CreateAnonymousObject(x, fields, format)).ToList());
    }

    [HttpGet("{id}", Name = nameof(UsersController) + nameof(GetOne))]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetOne(
        int id,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (id == 0)
        {
            ModelState.AddModelError("id", $"id: {id}, Is not Valid");
            return BadRequest(ModelState);
        }
        var item = await _userService.GetOne(id);
        if (item == null)
            return NotFound(new { id, message = "NotFound" });

        var result = Utilities.CreateAnonymousObject(item, fields, format: format);
        return Ok(result);
    }

    [HttpPost]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status201Created)]
    public async Task<IActionResult> Create(
        [FromBody] UserCreateDto itemDto,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        var createdItem = await _userService.Create(itemDto);
        return CreatedAtRoute(nameof(UsersController) + nameof(GetOne), new { id = "idk how to route", fields, format }, createdItem);
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(typeof(ResponseTypeId), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(
        int id,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (id == 0)
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        var deletedItem = await _userService.Delete(id);
        if (deletedItem == null)
            return NotFound(new { id, message = "NotFound" });
        var result = Utilities.CreateAnonymousObject(deletedItem, fields, format);
        return Ok(result);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Update(
        int id,
        [FromBody] UserUpdateDto itemDto,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (id == 0)
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        if (!ModelState.IsValid)
        {
            ModelState.AddModelError("item", "ModelState is not valid");
            return BadRequest(ModelState);
        }
        var updatedItem = await _userService.Update(id, itemDto);
        if (updatedItem == null)
            return NotFound(new { id, message = "NotFound" });
        var result = Utilities.CreateAnonymousObject(updatedItem, fields, format);
        return Ok(result);
    }

    [HttpPatch("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> UpdatePartial(
        int id,
        JsonPatchDocument<UserUpdateDto> patchDto)
    {
        if (id == 0)
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        if (!ModelState.IsValid)
        {
            ModelState.AddModelError("item", "ModelState is not valid");
            return BadRequest(ModelState);
        }
        var updateDto = new UserUpdateDto();
        patchDto.ApplyTo(updateDto, ModelState);
        var updatedItem = await _userService.Update(id, updateDto);
        if (updatedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return NoContent();
    }
}
