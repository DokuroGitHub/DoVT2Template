using _0.Share.Commons;
using _0.Share.IServices.Beta;
using _0.Share.ViewModels.User;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers.v1_1;

[ApiController]
[ApiVersion("1.1")]
[Route("api/v{version:apiVersion}/my-profile")]
public class MyProfileController : ControllerBase
{
    private readonly IMyProfileService _myUserService;
    private readonly IMapper _mapper;

    public MyProfileController(
        IMyProfileService userService,
        IMapper mapper)
    {
        _myUserService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    [Authorize]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetMyProfile(
    [FromQuery] string? fields = null,
    [FromQuery] string? format = null)
    {
        var item = await _myUserService.GetMyProfile();
        if (item == null)
            return NotFound(new { message = "NotFound" });
        var result = Utilities.CreateAnonymousObject(item, fields, format);
        return Ok(result);
    }

    [HttpPut]
    [Authorize]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> UpdateMyProfile(
        [FromBody] UserUpdateDto dto,
        [FromQuery] string? fields = null,
        [FromQuery] string? format = null)
    {
        if (string.IsNullOrEmpty(dto.FirstName) && string.IsNullOrEmpty(dto.LastName))
        {
            ModelState.AddModelError(nameof(dto.FirstName), "FirstName is not valid");
            ModelState.AddModelError(nameof(dto.LastName), "LastName is not valid");
            return UnprocessableEntity(ModelState);
        }
        var updatedItem = await _myUserService.UpdateMyProfile(dto);
        var result = Utilities.CreateAnonymousObject(updatedItem, fields, format);
        return Ok(result);
    }

    [HttpPatch]
    [Authorize]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> UpdateMyProfilePartial(
        JsonPatchDocument<UserUpdateDto> patchDto)
    {
        var updateDto = new UserUpdateDto();
        patchDto.ApplyTo(updateDto, ModelState);
        if (string.IsNullOrEmpty(updateDto.FirstName) && string.IsNullOrEmpty(updateDto.LastName))
        {
            ModelState.AddModelError(nameof(updateDto.FirstName), "FirstName is not valid");
            ModelState.AddModelError(nameof(updateDto.LastName), "LastName is not valid");
            return UnprocessableEntity(ModelState);
        }
        var updatedItem = await _myUserService.UpdateMyProfile(updateDto);
        return NoContent();
    }

}
