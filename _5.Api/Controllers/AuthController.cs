﻿using _0.Share.IServices;
using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers;

[ApiController]
[ApiVersionNeutral]
[Route("api/v{version:apiVersion}/[controller]")]
public class AuthController : ControllerBase
{
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }

    [HttpPost("login")]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> Login([FromBody] LoginRequest model)
    {
        if (string.IsNullOrEmpty(model.Username))
            ModelState.AddModelError(nameof(model.Username), "Is not Valid");
        if (string.IsNullOrEmpty(model.Password))
            ModelState.AddModelError(nameof(model.Password), "Is not Valid");
        if (!ModelState.IsValid)
            return UnprocessableEntity(ModelState);

        var loginResponse = await _authService.LoginAsync(model);
        if (loginResponse == null || string.IsNullOrEmpty(loginResponse.Token))
        {
            ModelState.AddModelError("LoginRequest", "Failed to login");
            return BadRequest(ModelState);
        }
        return Ok(loginResponse);
    }

    [HttpPost("register")]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    [ProducesResponseType(typeof(UserFlatDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Register([FromBody] RegisterRequest dto)
    {
        if (string.IsNullOrEmpty(dto.Email))
            ModelState.AddModelError(nameof(dto.Email), "Is not Valid");
        bool ifUserNameUnique = await _authService.IsUniqueUserAsync(dto.Email);
        if (!ifUserNameUnique)
            ModelState.AddModelError(nameof(dto.Email), "Email already exists");
        if (string.IsNullOrEmpty(dto.Password))
            ModelState.AddModelError(nameof(dto.Password), "Is not Valid");
        if (dto.Password != dto.ConfirmPassword)
            ModelState.AddModelError(nameof(dto.ConfirmPassword), "Is not Valid");
        if (!ModelState.IsValid)
            return UnprocessableEntity(ModelState);
        var user = await _authService.RegisterAsync(dto);
        if (user == null || user.Id == default)
        {
            ModelState.AddModelError("RegisterationRequest", "Failed to register");
            return BadRequest(ModelState);
        }
        return Ok(user);
    }

    // Admin Login
    [HttpPost("admin-login")]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> AdminLoginAsync([FromBody] LoginRequest model)
    {
        if (string.IsNullOrEmpty(model.Username))
            ModelState.AddModelError(nameof(model.Username), "Is not Valid");
        if (string.IsNullOrEmpty(model.Password))
            ModelState.AddModelError(nameof(model.Password), "Is not Valid");
        if (!ModelState.IsValid)
            return UnprocessableEntity(ModelState);
        var loginResponse = await _authService.AdminLoginAsync(model);
        if (loginResponse == null || string.IsNullOrEmpty(loginResponse.Token))
        {
            ModelState.AddModelError("LoginRequest", "Failed to login");
            return BadRequest(ModelState);
        }
        return Ok(loginResponse);
    }
}
