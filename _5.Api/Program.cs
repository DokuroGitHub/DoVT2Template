using _0.Share;
using _3.DataAccessLayer;
using _4.BusinessLayer;
using _5.Api;

var builder = WebApplication.CreateBuilder(args);

// appsettings.json
var _appsettings = builder.Configuration.Get<Appsettings>() ?? new Appsettings();
// add services
builder.Services.AddDataAccessLayerServices(_appsettings);
builder.Services.AddBusinessLayerServices(_appsettings);
builder.Services.AddApiServices(_appsettings);

var app = builder.Build();
// use middlewares
app.UseApiMiddlewares();
app.Run();

Console.WriteLine("hello there");

// https://stackoverflow.com/questions/69991983/deps-file-missing-for-dotnet-6-integration-tests
public partial class Program { }