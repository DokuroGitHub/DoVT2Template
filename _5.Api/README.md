# Api

```json
  "ConnectionStrings": {
    "DokuroSQLConnectionString": "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DoVT2Template;Integrated Security=True;Encrypt=False"
  },

  "ConnectionStrings": {
    "DokuroSQLConnectionString": "Data Source=DESKTOP-2T86NCF;Initial Catalog=DoVT2Template;Integrated Security=True;Encrypt=False"
  },
```

```bash
# run
dotnet build
dotnet run
dotnet watch
#
dotnet new webapi

dotnet add reference "../_4.BusinessLayer/_4.BusinessLayer.csproj"

dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.AspNetCore.Mvc.Versioning
dotnet add package Microsoft.AspNetCore.Mvc.Versioning.ApiExplorer
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
dotnet add package Microsoft.EntityFrameworkCore.InMemory
dotnet add package FluentValidation
dotnet add package FluentValidation.AspNetCore
dotnet add package Swashbuckle.AspNetCore
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson

#
```
