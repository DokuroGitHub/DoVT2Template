﻿using _0.Share;
using _5.Api.Middlewares;
using _5.Api.Validations;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Diagnostics;
using System.Text;

namespace _5.Api;

public static partial class ServicesExtensions
{
    public static IServiceCollection AddApiServices(this IServiceCollection services, Appsettings appsettings)
    {
        // add api versioning
        services.AddApiVersioning(options =>
        {
            options.DefaultApiVersion = new ApiVersion(1, 0);
            options.AssumeDefaultVersionWhenUnspecified = true;
            options.ReportApiVersions = true;
        });
        services.AddVersionedApiExplorer(options =>
        {
            options.GroupNameFormat = "'v'VVV";
            options.SubstituteApiVersionInUrl = true;
        });

        // add cors
        services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy", builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        });

        // add controllers
        services.AddControllers(options =>
        {
            options.CacheProfiles.Add("Default30",
                new CacheProfile()
                {
                    Duration = 30
                });
            options.InputFormatters.Insert(0, MyJPIF.GetJsonPatchInputFormatter());
        }).AddNewtonsoftJson();//.AddXmlSerializerFormatters();
        services.AddResponseCaching();

        // add health checks
        services.AddHealthChecks();
        // add validations
        services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);
        services.AddFluentValidationAutoValidation();
        services.AddFluentValidationClientsideAdapters();
        services.AddValidatorsFromAssemblyContaining<UserSearchValidation>();

        // add middlewares
        services.AddSingleton(appsettings);
        services.AddSingleton<ExceptionMiddleware>();
        services.AddSingleton<PerformanceMiddleware>();
        services.AddSingleton<Stopwatch>();
        services.AddScoped<ValidationFilterAttribute>();

        // add jwt authentication
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(o =>
        {
            o.TokenValidationParameters = new TokenValidationParameters
            {
                ValidIssuer = appsettings.Jwt.Issuer,
                ValidAudience = appsettings.Jwt.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appsettings.Jwt.Key)),
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = true
            };
        });

        // add authorization
        services.AddAuthorization();

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            // Add JWT authentication support in Swagger
            var securityScheme = new OpenApiSecurityScheme
            {
                Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
                Scheme = "bearer",
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            };

            options.AddSecurityDefinition("Bearer", securityScheme);

            var securityRequirement = new OpenApiSecurityRequirement
            {
                {
                    securityScheme, new[] { "Bearer" }
                }
            };

            options.AddSecurityRequirement(securityRequirement);

            // add swagger doc
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "DoVT2Template V1",
                Description = "Tempalate API",
            });
            options.SwaggerDoc("v1.1", new OpenApiInfo
            {
                Version = "v1.1",
                Title = "DoVT2Template V1.1",
                Description = "Tempalate API",
            });
        });

        return services;
    }

    public static WebApplication UseApiMiddlewares(this WebApplication app)
    {
        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("v1/swagger.json", "DoVT2Template v1");
                options.SwaggerEndpoint("v1.1/swagger.json", "DoVT2Template beta");
            });
        }
        app.UseExceptionMiddleware();
        app.UsePerformanceMiddleware();
        app.MapHealthChecks("/healthchecks");
        app.UseHttpsRedirection();
        app.UseAuthentication();
        app.UseAuthorization();

        app.MapControllers();

        return app;
    }
}
