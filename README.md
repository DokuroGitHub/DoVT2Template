# DoVT2Template

```bash
cd _2.Domain2Db
# change DefaultConnectionString at _2.Domain2Db.appsettings.json.ConnectionStrings.DefaultConnectionString
dotnet ef migrations add InitDb
dotnet ef database update
cd ..
cd _5.Api
# change DefaultConnectionString at _5.Api.appsettings.Development.json.ConnectionStrings.DefaultConnectionString
dotnet watch
cd ..
cd _7.ApiClient
dotnet watch
cd ..
cd _8.WebApp
dotnet watch

#
cd _5.Api
dotnet watch --launch-profile https
cd ..
cd _7.ApiClient
dotnet watch --launch-profile https
cd ..
cd _8.WebApp
dotnet watch --launch-profile https

#
dotnet new gitignore
git init
git add .
git commit -m "c1 Init"
git push --set-upstream https://gitlab.com/DokuroGitHub/DoVT2Template.git master


curl -k https://localhost:7001/api/users/paged-items
curl -k https://localhost:7001/api/users/advanced-search

curl -k https://localhost:7002/users/paged-items
curl -k https://localhost:7002/users/advanced-search

https://localhost:7003

```
