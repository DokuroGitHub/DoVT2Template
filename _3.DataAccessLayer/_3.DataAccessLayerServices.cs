﻿using _2.Domain2Db;
using _3.DataAccessLayer.Repositories;
using _3.DataAccessLayer.Repositories.IRepositories;
using _3.DataAccessLayer.Services;
using _3.DataAccessLayer.Services.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace _3.DataAccessLayer;

public static partial class ServicesExtensions
{
    public static IServiceCollection AddDataAccessLayerServices(this IServiceCollection services, _0.Share.Appsettings appsettings)
    {
        // mapper
        services.AddAutoMapper(typeof(MappingConfig).Assembly);

        // add services
        services.AddHttpContextAccessor(); // for CurrentUserService
        services.AddScoped<ICurrentUserService, CurrentUserService>();
        services.AddScoped<IJwtService, JwtService>();

        // Repositories
        services.AddScoped<IUserRepository, UserRepository>();

        // UnitOfWork
        services.AddScoped<IUnitOfWork, UnitOfWork>();

        // DbContext
        services.AddDbContext<ApplicationDbContext>(option => option.UseSqlServer(appsettings.ConnectionStrings.DefaultSQLConnectionString));
        // services.AddDbContext<ApplicationDbContext>(option => option.UseInMemoryDatabase("DokuroDb"));
        services.AddMemoryCache();

        return services;
    }
}
