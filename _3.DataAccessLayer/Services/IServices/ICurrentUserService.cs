﻿namespace _3.DataAccessLayer.Services.IServices;

public interface ICurrentUserService
{
    public int CurrentUserId { get; }
    public bool IsAdmin { get; }
}
