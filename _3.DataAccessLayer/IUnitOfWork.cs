﻿using _3.DataAccessLayer.Repositories.IRepositories;

namespace _3.DataAccessLayer;

public interface IUnitOfWork : IDisposable
{
    // repositories
    public IUserRepository UserRepository { get; }
    // save changes
    int SaveChanges();
    Task<int> SaveChangesAsync();
    // transaction
    void BeginTransaction();
    // commit
    void Commit();
    Task CommitAsync();
    // rollback
    void Rollback();
    Task RollbackAsync();
}
