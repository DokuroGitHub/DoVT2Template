﻿using _0.Share.ViewModels;
using _0.Share.ViewModels.User;
using _1.Domain.Entities;
using AutoMapper;

namespace _3.DataAccessLayer;

public class MappingConfig : Profile
{
    public MappingConfig()
    {
        // PagedResult + ItemResult
        CreateMap(typeof(PagedResult<>), typeof(PagedResult<>));
        CreateMap(typeof(ItemResult<>), typeof(ItemResult<>));

        // User Dto
        CreateMap<User, UserDto>().MaxDepth(2).ReverseMap();
        CreateMap<User, UserCreateDto>().MaxDepth(2).ReverseMap()
            .ForMember(dest => dest.FirstName, opt => opt.Condition(src => src.FirstName != null))
            .ForMember(dest => dest.LastName, opt => opt.Condition(src => src.LastName != null))
            .ForMember(dest => dest.Email, opt => opt.Condition(src => src.Email != null))
            .ForMember(dest => dest.Username, opt => opt.Condition(src => src.Username != null))
            .ForMember(dest => dest.HashPassword, opt => opt.Condition(src => src.HashPassword != null))
            .ForMember(dest => dest.Role, opt => opt.Condition(src => src.Role != null))
            .ForMember(dest => dest.ManagerId, opt => opt.Condition(src => src.ManagerId != null));
        CreateMap<User, UserUpdateDto>().MaxDepth(2).ReverseMap()
            .ForMember(dest => dest.FirstName, opt => opt.Condition(src => src.FirstName != null))
            .ForMember(dest => dest.LastName, opt => opt.Condition(src => src.LastName != null));
        CreateMap<User, UserFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<User, UserFlatRefDto>().MaxDepth(2).ReverseMap();
    }
}
