﻿using _0.Share.Enums;
using _0.Share.ViewModels;
using _1.Domain.Entities;
using _2.Domain2Db;
using _3.DataAccessLayer.Commons;
using _3.DataAccessLayer.Repositories.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace _3.DataAccessLayer.Repositories;

public class UserRepository : GenericRepository<User>, IUserRepository
{
    private readonly ApplicationDbContext _context;

    public UserRepository(
        ApplicationDbContext context)
        : base(
            context)
    {
        _context = context;
    }

    public async Task<bool> CheckExistByEmail(string email) => await _context.Users.AnyAsync(x => x.Email == email);

    public async Task<bool> CheckExistByUsername(string username) => await _context.Users.AnyAsync(x => x.Username == username);

    public async Task<User> FindByEmail(string email)
    {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
        if (user == null)
            throw new RepositoryException("Incorrect Email!!!");
        return user;
    }

    public async Task<User> FindByUsername(string username)
    {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);
        if (user == null)
            throw new RepositoryException("Incorrect username!!!");
        return user;
    }

    public async Task<ItemResult<ReportUsersByStartEnd>> GetPagedReportUsersByStartEndAsync(
        DateTime start,
        DateTime end,
        int pageIndex = 1,
        int pageSize = 10)
    {
        var dayCount = (end - start).Days + 1;
        if (dayCount < 0)
            throw new RepositoryException("Start date must be less than end date.");
        if (pageIndex < 1)
            throw new RepositoryException("Page index must be greater than 0.");
        if (pageSize < 1)
            throw new RepositoryException("Page size must be greater than 0.");

        var query = _dbSet.AsQueryable().AsNoTracking();

        // filter
        query = query.Where(x =>
            x.Role != UserRole.Admin &&
            x.CreatedAt.Date >= start.Date &&
            x.CreatedAt.Date <= end.Date);

        var reportUsersByDates = new List<ReportUsersByDate>();
        // 5 days
        // index = 1, size = 2
        // i [ 0 + 1 ] // size * (index - 1) -> < min(5, 2) // 2 = size * index
        // i [ 2 + 3 ] // size * (index - 1) -> < min(5, 4) // 4 = size * index
        // i [ 4 ] // size * (index - 1) -> < min(5, 6) // 6 = size * index
        for (int i = pageSize * (pageIndex - 1); i < int.Min(dayCount, pageSize * pageIndex); i++)
        {
            // 0, 1
            // 2, 3
            // 4
            var date = start.AddDays(i);
            reportUsersByDates.Add(new ReportUsersByDate
            {
                Date = date,
                UserCount = await query.Where(x => x.CreatedAt.Date == date.Date).CountAsync(),
            });
        }
        var result = new ItemResult<ReportUsersByStartEnd>(
            item: new ReportUsersByStartEnd()
            {
                Start = start,
                End = end,
                UserCount = await query.CountAsync(),
                ReportUsersByDates = reportUsersByDates
            },
            count: (end - start).Days + 1,
            pageIndex: pageIndex,
            pageSize: pageSize
        );
        return result;
    }
}
