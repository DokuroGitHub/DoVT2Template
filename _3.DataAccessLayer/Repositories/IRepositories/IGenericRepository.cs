﻿using System.Linq.Expressions;
using _0.Share.ViewModels;
using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore.Query;

namespace _3.DataAccessLayer.Repositories.IRepositories;

public interface IGenericRepository<TEntity> where TEntity : BaseEntity
{
    # region Read
    PagedResult<TEntity> GetPagedResult(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        int pageIndex = 1,
        int pageSize = 10,
        bool tracked = false);
    Task<PagedResult<TEntity>> GetPagedResultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        int pageIndex = 1,
        int pageSize = 10,
        bool tracked = false);
    IEnumerable<TEntity> GetAll(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false);
    Task<IEnumerable<TEntity>> GetAllAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false);
    TEntity? FirstOrdDefault(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false);
    Task<TEntity?> FirstOrDefaultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false);
    # endregion Read
    #region Create + Update + Delete
    void Add(TEntity entity);
    Task AddAsync(TEntity entity);
    void AddRange(IEnumerable<TEntity> entities);
    Task AddRangeAsync(IEnumerable<TEntity> entities);
    void Update(TEntity entity);
    void UpdateRange(IEnumerable<TEntity> entities);
    void Remove(TEntity entity);
    void Remove(Expression<Func<TEntity, bool>> filter);
    Task RemoveAsync(Expression<Func<TEntity, bool>> filter);
    void RemoveRange(IEnumerable<TEntity> entities);
    void RemoveRange(Expression<Func<TEntity, bool>> filter);
    Task RemoveRangeAsync(Expression<Func<TEntity, bool>> filter);
    # endregion Create + Update + Delete
    #region Any + Count + LongCount
    bool Any(Expression<Func<TEntity, bool>>? filter = null);
    Task<bool> AnyAsync(Expression<Func<TEntity, bool>>? filter = null);
    int Count(Expression<Func<TEntity, bool>>? filter = null);
    Task<int> CountAsync(Expression<Func<TEntity, bool>>? filter = null);
    double LongCount(Expression<Func<TEntity, bool>>? filter = null);
    Task<double> LongCountAsync(Expression<Func<TEntity, bool>>? filter = null);
    #endregion Any + Count + LongCount
}
