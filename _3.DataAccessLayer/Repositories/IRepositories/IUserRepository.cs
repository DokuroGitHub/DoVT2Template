﻿using _0.Share.ViewModels;
using _1.Domain.Entities;

namespace _3.DataAccessLayer.Repositories.IRepositories;

public interface IUserRepository : IGenericRepository<User>
{
    Task<bool> CheckExistByEmail(string email);
    Task<bool> CheckExistByUsername(string username);
    Task<User> FindByEmail(string email);
    Task<User> FindByUsername(string username);
    Task<ItemResult<ReportUsersByStartEnd>> GetPagedReportUsersByStartEndAsync(
        DateTime start,
        DateTime end,
        int pageIndex = 1,
        int pageSize = 10);
}
