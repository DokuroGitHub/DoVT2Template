﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using _3.DataAccessLayer.Repositories.IRepositories;
using _2.Domain2Db;
using _3.DataAccessLayer.Commons;
using _0.Share.ViewModels;
using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore.Query;

namespace _3.DataAccessLayer.Repositories;

public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
{
    protected DbSet<TEntity> _dbSet;
    //
    public GenericRepository(
        ApplicationDbContext context)
    {
        _dbSet = context.Set<TEntity>();
    }

    # region Read

    public PagedResult<TEntity> GetPagedResult(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        int pageIndex = 1,
        int pageSize = 10,
        bool tracked = false)
    {
        var query = _dbSet.AsQueryable();

        // filter
        query = query.Filter(
            filter: filter,
            include: include,
            orderBy: orderBy,
            pageIndex: pageIndex,
            pageSize: pageSize,
            tracked: tracked);

        var count = query.Count();
        var items = query.ToList();

        var result = new PagedResult<TEntity>(
            items: items,
            count: count,
            pageIndex: pageIndex,
            pageSize: pageSize
        );
        return result;
    }

    public async Task<PagedResult<TEntity>> GetPagedResultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        int pageIndex = 1,
        int pageSize = 10,
        bool tracked = false)
    {
        var query = _dbSet.AsQueryable();

        // filter
        query = query.Filter(
            filter: filter,
            include: include,
            orderBy: orderBy,
            pageIndex: pageIndex,
            pageSize: pageSize,
            tracked: tracked);

        var count = await query.CountAsync();
        var items = await query.ToListAsync();

        var result = new PagedResult<TEntity>(
            items: items,
            count: count,
            pageIndex: pageIndex,
            pageSize: pageSize
        );
        return result;
    }

    public IEnumerable<TEntity> GetAll(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false)
    {
        var query = _dbSet.AsQueryable();

        // filter
        query = query.Filter(
            filter: filter,
            include: include,
            orderBy: orderBy,
            tracked: tracked);

        var items = query.ToList();
        return items;
    }

    public async Task<IEnumerable<TEntity>> GetAllAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false)
    {
        var query = _dbSet.AsQueryable();

        // filter
        query = query.Filter(
            filter: filter,
            include: include,
            orderBy: orderBy,
            tracked: tracked);

        var items = await query.ToListAsync();
        return items;
    }

    public TEntity? FirstOrdDefault(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false)
    {
        var query = _dbSet.AsQueryable();

        // filter
        query = query.Filter(
            filter: filter,
            include: include,
            orderBy: orderBy,
            tracked: tracked);

        var item = query.FirstOrDefault();
        return item;
    }

    public async Task<TEntity?> FirstOrDefaultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        bool tracked = false)
    {
        var query = _dbSet.AsQueryable();

        // filter
        query = query.Filter(
            filter: filter,
            include: include,
            orderBy: orderBy,
            tracked: tracked);

        var item = await query.FirstOrDefaultAsync();
        return item;
    }

    #endregion Read

    #region Create + Update + Delete

    public void Add(TEntity entity)
    {
        _dbSet.Add(entity);
    }

    public async Task AddAsync(TEntity entity)
    {
        await _dbSet.AddAsync(entity);
    }

    public void AddRange(IEnumerable<TEntity> entities)
    {
        _dbSet.AddRange(entities);
    }

    public async Task AddRangeAsync(IEnumerable<TEntity> entities)
    {
        await _dbSet.AddRangeAsync(entities);
    }

    public void Update(TEntity entity)
    {
        _dbSet.Update(entity);
    }

    public void UpdateRange(IEnumerable<TEntity> entities)
    {
        _dbSet.UpdateRange(entities);
    }

    public void Remove(TEntity entity)
    {
        _dbSet.Remove(entity);
    }

    public void Remove(Expression<Func<TEntity, bool>> filter)
    {
        var entity = _dbSet.FirstOrDefault(filter);
        if (entity == null)
            throw new RepositoryException("Entity not found");
        _dbSet.Remove(entity);
    }

    public async Task RemoveAsync(Expression<Func<TEntity, bool>> filter)
    {
        var entity = await _dbSet.FirstOrDefaultAsync(filter);
        if (entity == null)
            throw new RepositoryException("Entity not found");
        _dbSet.Remove(entity);
    }

    public void RemoveRange(IEnumerable<TEntity> entities)
    {
        _dbSet.RemoveRange(entities);
    }

    public void RemoveRange(Expression<Func<TEntity, bool>> filter)
    {
        var entities = _dbSet.Where(filter).ToList();
        if (entities.Count == 0)
            throw new RepositoryException("Entities not found");
        _dbSet.RemoveRange(entities);
    }

    public async Task RemoveRangeAsync(Expression<Func<TEntity, bool>> filter)
    {
        var entities = await _dbSet.Where(filter).ToListAsync();
        if (entities.Count == 0)
            throw new RepositoryException("Entities not found");
        _dbSet.RemoveRange(entities);
    }

    #endregion Create + Update + Delete

    #region Any + Count + LongCount

    public bool Any(Expression<Func<TEntity, bool>>? filter = null)
    {
        return filter == null
            ? _dbSet.Any()
            : _dbSet.Any(filter);
    }

    public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>>? filter = null)
    {
        return filter == null
            ? await _dbSet.AnyAsync()
            : await _dbSet.AnyAsync(filter);
    }

    public int Count(Expression<Func<TEntity, bool>>? filter = null)
    {
        return filter == null
            ? _dbSet.Count()
            : _dbSet.Count(filter);
    }

    public async Task<int> CountAsync(Expression<Func<TEntity, bool>>? filter = null)
    {
        return filter == null
            ? await _dbSet.CountAsync()
            : await _dbSet.CountAsync(filter);
    }

    public double LongCount(Expression<Func<TEntity, bool>>? filter = null)
    {
        return filter == null
            ? _dbSet.LongCount()
            : _dbSet.LongCount(filter);
    }

    public async Task<double> LongCountAsync(Expression<Func<TEntity, bool>>? filter = null)
    {
        return filter == null
            ? await _dbSet.LongCountAsync()
            : await _dbSet.LongCountAsync(filter);
    }

    #endregion Any + Count + LongCount
}
