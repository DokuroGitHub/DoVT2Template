# \_3.DataAccessLayer

run

```bash
dotnet build
```

create csproj

```bash
dotnet new classlib

dotnet add reference "../_2.Domain2Db/_2.Domain2Db.csproj"

dotnet add package AutoMapper
dotnet add package AutoMapper.Extensions.Microsoft.Dependencyinjection
dotnet add package BCrypt.Net-Next
dotnet add package Microsoft.AspNetCore.Http

#
```
